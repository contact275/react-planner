'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _md = require('react-icons/md');

var _fa = require('react-icons/fa');

var _gi = require('react-icons/gi');

var _toolbarButton = require('./toolbar-button');

var _toolbarButton2 = _interopRequireDefault(_toolbarButton);

var _toolbarSaveButton = require('./toolbar-save-button');

var _toolbarSaveButton2 = _interopRequireDefault(_toolbarSaveButton);

var _toolbarLoadButton = require('./toolbar-load-button');

var _toolbarLoadButton2 = _interopRequireDefault(_toolbarLoadButton);

var _reactIf = require('../../utils/react-if');

var _reactIf2 = _interopRequireDefault(_reactIf);

var _constants = require('../../constants');

var _sharedStyle = require('../../shared-style');

var SharedStyle = _interopRequireWildcard(_sharedStyle);

var _wall2IconSleek = require('./image/wall2-icon-sleek-1.png');

var _wall2IconSleek2 = _interopRequireDefault(_wall2IconSleek);

var _reception2IconSleek = require('./image/reception2-icon-sleek-1.png');

var _reception2IconSleek2 = _interopRequireDefault(_reception2IconSleek);

var _undoIconSleek = require('./image/undo-icon-sleek-1.png');

var _undoIconSleek2 = _interopRequireDefault(_undoIconSleek);

var _doneIconSleek = require('./image/done-icon-sleek-1.png');

var _doneIconSleek2 = _interopRequireDefault(_doneIconSleek);

var _landlord3IconSleek = require('./image/landlord3-icon-sleek-1.png');

var _landlord3IconSleek2 = _interopRequireDefault(_landlord3IconSleek);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var iconTextStyle = {
  fontSize: '19px',
  textDecoration: 'none',
  fontWeight: 'bold',
  margin: '0px',
  userSelect: 'none'
};

var iconCenterStyle = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  msTransform: 'translate(-50%, -50%)',
  transform: 'translate(-50%, -50%)'
};

var Icon2D = function Icon2D(_ref) {
  var style = _ref.style;
  return _react2.default.createElement(
    'p',
    { style: _extends({}, iconTextStyle, style) },
    '2D'
  );
};
var Icon3D = function Icon3D(_ref2) {
  var style = _ref2.style;
  return _react2.default.createElement(
    'p',
    { style: _extends({}, iconTextStyle, style) },
    '3D'
  );
};

var ASIDE_STYLE = {
  backgroundColor: SharedStyle.PRIMARY_COLOR.white,
  padding: '10px'
};

var sortButtonsCb = function sortButtonsCb(a, b) {
  if (a.index === undefined || a.index === null) {
    a.index = Number.MAX_SAFE_INTEGER;
  }

  if (b.index === undefined || b.index === null) {
    b.index = Number.MAX_SAFE_INTEGER;
  }

  return a.index - b.index;
};

var mapButtonsCb = function mapButtonsCb(el, ind) {
  return _react2.default.createElement(
    _reactIf2.default,
    {
      key: ind,
      condition: el.condition,
      style: { position: 'relative' }
    },
    el.dom
  );
};

var Toolbar = function (_Component) {
  _inherits(Toolbar, _Component);

  function Toolbar(props, context) {
    _classCallCheck(this, Toolbar);

    var _this = _possibleConstructorReturn(this, (Toolbar.__proto__ || Object.getPrototypeOf(Toolbar)).call(this, props, context));

    _this.state = {
      selectedToolbarIndex: -1
    };
    return _this;
  }

  _createClass(Toolbar, [{
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      // handle area changed. deactivate drawing tool
      if (this.props.areasChanged) {
        this.setState({ selectedToolbarIndex: -1 });
        this.props.handleAreasChanged(false);
      }

      return this.props.state.mode !== nextProps.state.mode || this.props.height !== nextProps.height || this.props.width !== nextProps.width ||
      // this.props.state.alterate !== nextProps.state.alterate ||
      this.state.selectedToolbarIndex !== nextState.selectedToolbarIndex;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          width = _props.width,
          height = _props.height,
          toolbarButtons = _props.toolbarButtons,
          allowProjectFileSupport = _props.allowProjectFileSupport,
          _context = this.context,
          projectActions = _context.projectActions,
          viewer3DActions = _context.viewer3DActions,
          translator = _context.translator,
          linesActions = _context.linesActions,
          holesActions = _context.holesActions,
          areaActions = _context.areaActions;


      var mode = this.props.state.get('mode');
      var alterate = this.props.state.get('alterate');
      // let alterateColor = alterate ? SharedStyle.MATERIAL_COLORS[500].orange : '';
      var scene = this.props.state.scene;

      var selectedToolbarIndex = this.state.selectedToolbarIndex;

      var sorter = [
      // {
      //   index: 0, condition: allowProjectFileSupport, dom: <ToolbarButton
      //     active={false}
      //     tooltip={translator.t('New project')}
      //     onClick={event => confirm(translator.t('Would you want to start a new Project?')) ? projectActions.newProject() : null}>
      //     <FaFile />
      //   </ToolbarButton>
      // },
      // {
      //   index: 1, condition: allowProjectFileSupport,
      //   dom: <ToolbarSaveButton state={state} />
      // },
      // {
      //   index: 2, condition: allowProjectFileSupport,
      //   dom: <ToolbarLoadButton state={state} />
      // },

      {
        index: 0, condition: true,
        dom: _react2.default.createElement(_toolbarButton2.default, {
          active: false,
          selected: selectedToolbarIndex === 0,
          tooltip: translator.t('Draw Wall'),
          onClick: function onClick(event) {
            projectActions.unselectAll();
            //check if currently selected 
            if (selectedToolbarIndex !== 0) {
              _this2.context.linesActions.selectToolDrawingLine('wall');
              _this2.setState({ selectedToolbarIndex: 0 });
            } else {
              if (mode === 'MODE_DRAWING_LINE') {
                projectActions.undo();
              }
              projectActions.setMode(_constants.MODE_IDLE);
              _this2.setState({ selectedToolbarIndex: -1 });
            }
          },
          activeIcon: _react2.default.createElement('img', { src: _wall2IconSleek2.default, width: 28, height: 28, style: iconCenterStyle, alt: "wallIcon" }),
          inactiveIcon: _react2.default.createElement('img', { src: _wall2IconSleek2.default, width: 28, height: 28, style: iconCenterStyle, alt: "wallIcon2" })
        })
      }, {
        index: 1, condition: true,
        dom: _react2.default.createElement(_toolbarButton2.default, {
          active: false,
          selected: selectedToolbarIndex === 1,
          tooltip: translator.t('Insert Reception Area'),
          onClick: function onClick(event) {
            projectActions.unselectAll();

            //check if currently selected 
            if (selectedToolbarIndex !== 1) {
              _this2.context.holesActions.selectToolDrawingHole('door');
              _this2.setState({ selectedToolbarIndex: 1 });
              // console.log(mode)
            } else {
              projectActions.setMode(_constants.MODE_IDLE);
              _this2.setState({ selectedToolbarIndex: -1 });
            }
          },
          activeIcon: _react2.default.createElement('img', { src: _reception2IconSleek2.default, width: 28, height: 28, style: iconCenterStyle, alt: "receptionIcon" }),
          inactiveIcon: _react2.default.createElement('img', { src: _reception2IconSleek2.default, width: 28, height: 28, style: iconCenterStyle, alt: "receptionIcon2" })
        })
      }, {
        index: 2, condition: true,
        dom: _react2.default.createElement(_toolbarButton2.default, {
          active: false,
          selected: selectedToolbarIndex === 2,
          tooltip: translator.t('Toggle Landlord Areas'),
          onClick: function onClick(event) {
            if (mode === 'MODE_DRAWING_LINE') {
              projectActions.undo();
            }
            projectActions.unselectAll();
            projectActions.setMode(_constants.MODE_IDLE);

            if (selectedToolbarIndex !== 2) {
              // select all areas by 2 steps
              // areas['area_id1']['selected'] = true
              // areas['area_id2']['selected'] = true
              // selected['areas'] = ['area_id1', 'area_id2']
              // find all area id and then select while in alterate state

              // bug : alternating between getting last areas and all areas 

              projectActions.setAlterateState();
              var defaultlayerID = 'layer-1'; // image layer
              var areas = scene.getIn(['layers', defaultlayerID, 'areas']); // get the areas
              var areasValues = areas.valueSeq();
              // console.log(areasValues)
              areasValues.forEach(function (_ref3) {
                var areaID = _ref3.id;

                areaActions.selectArea(defaultlayerID, areaID);
              });
              projectActions.setAlterateState();
              _this2.setState({ selectedToolbarIndex: 2 });
            } else {
              // projectActions.unselectAll();
              // projectActions.setMode( MODE_IDLE );
              _this2.setState({ selectedToolbarIndex: -1 });
            }
          },
          activeIcon: _react2.default.createElement('img', { src: _landlord3IconSleek2.default, width: 28, height: 28, style: iconCenterStyle, alt: "landlordIcon" }),
          inactiveIcon: _react2.default.createElement('img', { src: _landlord3IconSleek2.default, width: 28, height: 28, style: iconCenterStyle, alt: "landlordIcon2" })
        })
      }, {
        index: 3, condition: true, dom: _react2.default.createElement(_toolbarButton2.default, {
          active: false,
          tooltip: translator.t('Undo'),
          selected: selectedToolbarIndex === 3,
          onClick: function onClick(event) {
            projectActions.unselectAll();
            projectActions.undo();
            _this2.setState({ selectedToolbarIndex: -1 });
          },
          activeIcon: _react2.default.createElement('img', { src: _undoIconSleek2.default, width: 28, height: 28, style: iconCenterStyle, alt: "undoIcon" }),
          inactiveIcon: _react2.default.createElement('img', { src: _undoIconSleek2.default, width: 28, height: 28, style: iconCenterStyle, alt: "undoIcon2" })
        })
      }, {
        index: 4, condition: true, dom: _react2.default.createElement(_toolbarButton2.default, {
          active: false,
          selected: selectedToolbarIndex === 4,
          tooltip: translator.t('Stop Editing'),
          onClick: function onClick(event) {
            if (mode === 'MODE_DRAWING_LINE') {
              projectActions.undo();
            }
            projectActions.unselectAll();
            projectActions.setMode(_constants.MODE_IDLE);
            _this2.setState({ selectedToolbarIndex: -1 });
            // console.log(scene)
          },
          activeIcon: _react2.default.createElement('img', { src: _doneIconSleek2.default, width: 28, height: 28, style: iconCenterStyle, alt: "tickIcon" }),
          inactiveIcon: _react2.default.createElement('img', { src: _doneIconSleek2.default, width: 28, height: 28, style: iconCenterStyle, alt: "tickIcon2" })
        })
      }];

      // commented out to remove custom toolbar additions e.g. screenshot

      // sorter = sorter.concat(toolbarButtons.map((Component, key) => {
      //   return Component.prototype ? //if is a react component
      //     {
      //       condition: true,
      //       dom: React.createElement(Component, { mode, state, key })
      //     } :
      //     {                           //else is a sortable toolbar button
      //       index: Component.index,
      //       condition: Component.condition,
      //       dom: React.createElement(Component.dom, { mode, state, key })
      //     };
      // }));


      return _react2.default.createElement(
        'aside',
        { style: _extends({}, ASIDE_STYLE, { maxWidth: width, maxHeight: height }), className: 'toolbar' },
        sorter.sort(sortButtonsCb).map(mapButtonsCb)
      );
    }
  }]);

  return Toolbar;
}(_react.Component);

exports.default = Toolbar;


Toolbar.propTypes = {
  state: _propTypes2.default.object.isRequired,
  width: _propTypes2.default.number.isRequired,
  height: _propTypes2.default.number.isRequired,
  allowProjectFileSupport: _propTypes2.default.bool.isRequired,
  toolbarButtons: _propTypes2.default.array
};

Toolbar.contextTypes = {
  projectActions: _propTypes2.default.object.isRequired,
  viewer2DActions: _propTypes2.default.object.isRequired,
  viewer3DActions: _propTypes2.default.object.isRequired,
  linesActions: _propTypes2.default.object.isRequired,
  holesActions: _propTypes2.default.object.isRequired,
  areaActions: _propTypes2.default.object.isRequired,
  itemsActions: _propTypes2.default.object.isRequired,
  translator: _propTypes2.default.object.isRequired
};