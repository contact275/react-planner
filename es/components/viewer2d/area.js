var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import polylabel from 'polylabel';
import areapolygon from 'area-polygon';

var STYLE_TEXT = {
  textAnchor: 'middle',
  fontSize: '12px',
  fontFamily: '"Courier New", Courier, monospace',
  pointerEvents: 'none',
  fontWeight: 'bold',

  //http://stackoverflow.com/questions/826782/how-to-disable-text-selection-highlighting-using-css
  WebkitTouchCallout: 'none', /* iOS Safari */
  WebkitUserSelect: 'none', /* Chrome/Safari/Opera */
  MozUserSelect: 'none', /* Firefox */
  MsUserSelect: 'none', /* Internet Explorer/Edge */
  userSelect: 'none'
};

var STYLE_TEXT_ITALIC = {
  textAnchor: 'middle',
  fontSize: '10px',
  fontFamily: '"Courier New", Courier, monospace',
  pointerEvents: 'none',
  fontWeight: 'bold',
  fontStyle: 'italic',
  //http://stackoverflow.com/questions/826782/how-to-disable-text-selection-highlighting-using-css
  WebkitTouchCallout: 'none', /* iOS Safari */
  WebkitUserSelect: 'none', /* Chrome/Safari/Opera */
  MozUserSelect: 'none', /* Firefox */
  MsUserSelect: 'none', /* Internet Explorer/Edge */
  userSelect: 'none'
};

export default function Area(_ref, _ref2) {
  var scene = _ref.scene,
      layer = _ref.layer,
      area = _ref.area,
      catalog = _ref.catalog;
  var itemsActions = _ref2.itemsActions,
      projectActions = _ref2.projectActions,
      areaActions = _ref2.areaActions;

  var _useState = useState(false),
      _useState2 = _slicedToArray(_useState, 2),
      landlordBool = _useState2[0],
      setLandlordBool = _useState2[1];

  var rendered = catalog.getElement(area.type).render2D(area, layer);

  var renderedAreaSize = null;

  var renderedAreaType = null;

  var changeAreaType = function changeAreaType() {
    // update area properties
    // fix bug : this seems to  change all areas rather than 1 specific area

    // layer.setIn(['areas', area.id, "properties", "landlordBool"], !landlordBool)

    projectActions.unselectAll();

    var defaultlayerID = 'layer-1'; // image layer

    areaActions.selectArea(defaultlayerID, area.id);

    var properties = area.properties;
    properties = properties.setIn(['landlordBool'], !landlordBool);

    projectActions.setProperties(properties);

    projectActions.unselectAll();

    projectActions.setAlterateState();
    // console.log(scene)
    var areas = scene.getIn(['layers', defaultlayerID, 'areas']); // get the areas
    var areasValues = areas.valueSeq();
    // console.log(areasValues)
    areasValues.forEach(function (_ref3) {
      var areaID = _ref3.id;

      areaActions.selectArea(defaultlayerID, areaID);
    });
    projectActions.setAlterateState();

    setLandlordBool(!landlordBool);
  };

  var polygon = area.vertices.toArray().map(function (vertexID) {
    var _layer$vertices$get = layer.vertices.get(vertexID),
        x = _layer$vertices$get.x,
        y = _layer$vertices$get.y;

    return [x, y];
  });

  var polygonWithHoles = polygon;

  area.holes.forEach(function (holeID) {

    var polygonHole = layer.areas.get(holeID).vertices.toArray().map(function (vertexID) {
      var _layer$vertices$get2 = layer.vertices.get(vertexID),
          x = _layer$vertices$get2.x,
          y = _layer$vertices$get2.y;

      return [x, y];
    });

    polygonWithHoles = polygonWithHoles.concat(polygonHole.reverse());
  });

  var center = polylabel([polygonWithHoles], 1.0);

  // filter by the actual selected landlord area

  if (area.selected) {
    // let areaSize = areapolygon(polygon, false);

    // //subtract holes area
    // area.holes.forEach(areaID => {
    //   let hole = layer.areas.get(areaID);
    //   let holePolygon = hole.vertices.toArray().map(vertexID => {
    //     let {x, y} = layer.vertices.get(vertexID);
    //     return [x, y];
    //   });
    //   areaSize -= areapolygon(holePolygon, false);
    // });

    // renderedAreaSize = (
    //   <text x="0" y="0" transform={`translate(${center[0]} ${center[1]}) scale(1, -1)`} style={STYLE_TEXT}>
    //     {(areaSize / 10000 * 25).toFixed(0)} m{String.fromCharCode(0xb2)}
    //   </text>
    // )

    renderedAreaType = React.createElement(
      'g',
      { id: "area_polygon-" + area.id, className: 'base', x: '0', y: '0', transform: 'translate(' + center[0] + ' ' + center[1] + ') scale(1, -1)' },
      React.createElement('circle', { id: "area_circle-" + area.id, cx: '-30', cy: '0', className: landlordBool ? 'toggleLandlord' : 'toggleOffice', r: '16', onClick: changeAreaType.bind(this) }),
      React.createElement(
        'text',
        { x: '-30', y: '0', style: STYLE_TEXT_ITALIC },
        "click"
      ),
      React.createElement(
        'text',
        { x: '35', y: '0', style: STYLE_TEXT },
        landlordBool ? "landlord area" : "office area"
      ),
      React.createElement(
        'style',
        null,
        '.toggleLandlord { fill: #465767; stroke: black; }',
        '.toggleOffice { fill: #97c7bf; stroke: black; }'
      )
    );
  } else {

    renderedAreaType = React.createElement(
      'g',
      { id: "area_polygon-" + area.id, className: 'base', x: '0', y: '0', transform: 'translate(' + center[0] + ' ' + center[1] + ') scale(1, -1)' },
      React.createElement(
        'text',
        { x: '0', y: '0', style: STYLE_TEXT },
        landlordBool ? "landlord area" : "office area"
      )
    );
  }

  return React.createElement(
    'g',
    {
      'data-element-root': true,
      'data-prototype': area.prototype,
      'data-id': area.id,
      'data-selected': area.selected,
      'data-layer': layer.id
    },
    rendered,
    renderedAreaType
  );
}

Area.propTypes = {
  area: PropTypes.object.isRequired,
  layer: PropTypes.object.isRequired,
  catalog: PropTypes.object.isRequired
};

Area.contextTypes = {
  itemsActions: PropTypes.object.isRequired,
  projectActions: PropTypes.object.isRequired,
  areaActions: PropTypes.object.isRequired
};