import React from 'react';
import PropTypes from 'prop-types';
import { FaFolderOpen as IconLoad } from 'react-icons/fa';
import ToolbarButton from './toolbar-button';
import { browserUpload } from '../../utils/browser';

export default function ToolbarUploadButton(_ref, _ref2) {
  var state = _ref.state;
  var translator = _ref2.translator,
      projectActions = _ref2.projectActions;


  var loadImageFromFile = function loadImageFromFile(event) {
    event.preventDefault();
    browserUpload().then(function (data) {
      //   projectActions.loadProject(JSON.parse(data));
      console.log('loaded file');
    });
  };

  return React.createElement(
    ToolbarButton,
    { active: false, tooltip: translator.t("Upload Image"), onClick: loadImageFromFile },
    React.createElement(IconLoad, null)
  );
}

ToolbarUploadButton.propTypes = {
  state: PropTypes.object.isRequired
};

ToolbarUploadButton.contextTypes = {
  projectActions: PropTypes.object.isRequired,
  translator: PropTypes.object.isRequired
};