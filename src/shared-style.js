// COLORS
export const COLORS = {
  white: '#FFF',
  black: '#000'
};

export const MATERIAL_COLORS = {
  500: {
    amber: '#FFC107',
    blue_grey: '#607D8B',
    blue: '#2196F3',
    brown: '#795548',
    cyan: '#00BCD4',
    deep_orange: '#FF5722',
    deep_purple: '#673AB7',
    green: '#4CAF50',
    grey: '#9E9E9E',
    indigo: '#3F51B5',
    light_blue: '#03A9F4',
    light_green: '#8BC34A',
    lime: '#CDDC39',
    orange: '#FF9800',
    pink: '#E91E63',
    purple: '#9C27B0',
    red: '#F44336',
    teal: '#009688',
    yellow: '#FFEB3B'
  }
};

export const PRIMARY_COLOR = {
  main: '#28292D',
  alt: '#2E2F33',
  icon: '#C2C2C2',
  border: '1px solid #555',
  text_main: COLORS.white,
  text_alt: '#EBEBEB',
  input: '#55595C'
};

export const SECONDARY_COLOR = {
  main: '#1CA6FC',
  alt: '#005FAF',
  icon: '#1CA6FC',
  border: '1px solid #FFF'
};

export const MESH_SELECTED = '#99C3FB';

// light grey area in office
export const AREA_MESH_COLOR = {
  selected: '#e5a76dCC',
  unselected: '#dfdfdfCC'
};

// dark grey area in office
export const AREA_LANDLORD_MESH_COLOR = {
  // selected: '#ff6347',
  selected: 'url(#landlordSelectedDiagonalFill)',
  unselected: '#465767CC'
  // unselected: 'url(#landlordDiagonalFill)'
};

export const LINE_MESH_COLOR = {
  selected: '#465767',
  unselected: '#3f000f'
}

export const LINE_FILL_MESH_COLOR = {
  selected: '#e5a76d',
  unselected: 'url(#diagonalFill)'
}

// 100%	FF
// 95%	F2
// 90%	E6
// 85%	D9
// 80%	CC
// 75%	BF
// 70%	B3
// 65%	A6
// 60%	99
// 55%	8C
// 50%	80
// 45%	73
// 40%	66
// 35%	59
// 30%	4D
// 25%	40
// 20%	33
// 15%	26
// 10%	1A
// 5%	0D
// 0%	00