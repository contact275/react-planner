import React from 'react';
import PropTypes from 'prop-types';
import {FaFolderOpen as IconLoad} from 'react-icons/fa';
import ToolbarButton from './toolbar-button';
import {browserUpload}  from '../../utils/browser';

export default function ToolbarUploadButton({state}, {translator, projectActions}) {

  let loadImageFromFile = event => {
    event.preventDefault();
    browserUpload().then((data) => {
    //   projectActions.loadProject(JSON.parse(data));
    console.log('loaded file');
    });
  };

  return (
    <ToolbarButton active={false} tooltip={translator.t("Upload Image")} onClick={loadImageFromFile}>
      <IconLoad />
    </ToolbarButton>
  );
}

ToolbarUploadButton.propTypes = {
  state: PropTypes.object.isRequired,
};

ToolbarUploadButton.contextTypes = {
  projectActions: PropTypes.object.isRequired,
  translator: PropTypes.object.isRequired,
};
