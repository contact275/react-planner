import React, { useState } from 'react';
import PropTypes from 'prop-types';
import polylabel from 'polylabel';
import areapolygon from 'area-polygon';


const STYLE_TEXT = {
  textAnchor: 'middle',
  fontSize: '12px',
  fontFamily: '"Courier New", Courier, monospace',
  pointerEvents: 'none',
  fontWeight: 'bold',

  //http://stackoverflow.com/questions/826782/how-to-disable-text-selection-highlighting-using-css
  WebkitTouchCallout: 'none', /* iOS Safari */
  WebkitUserSelect: 'none', /* Chrome/Safari/Opera */
  MozUserSelect: 'none', /* Firefox */
  MsUserSelect: 'none', /* Internet Explorer/Edge */
  userSelect: 'none'
};

const STYLE_TEXT_ITALIC = {
  textAnchor: 'middle',
  fontSize: '10px',
  fontFamily: '"Courier New", Courier, monospace',
  pointerEvents: 'none',
  fontWeight: 'bold',
  fontStyle: 'italic',
  //http://stackoverflow.com/questions/826782/how-to-disable-text-selection-highlighting-using-css
  WebkitTouchCallout: 'none', /* iOS Safari */
  WebkitUserSelect: 'none', /* Chrome/Safari/Opera */
  MozUserSelect: 'none', /* Firefox */
  MsUserSelect: 'none', /* Internet Explorer/Edge */
  userSelect: 'none'
};


export default function Area({scene, layer, area, catalog}, {itemsActions, projectActions, areaActions}) {

  const [landlordBool, setLandlordBool] = useState(false);

  let rendered = catalog.getElement(area.type).render2D(area, layer);

  let renderedAreaSize = null;

  let renderedAreaType = null;
  
  const changeAreaType = () => {
    // update area properties
    // fix bug : this seems to  change all areas rather than 1 specific area
    
    // layer.setIn(['areas', area.id, "properties", "landlordBool"], !landlordBool)

    projectActions.unselectAll();
    
    let defaultlayerID = 'layer-1'; // image layer

    areaActions.selectArea(defaultlayerID, area.id);    

    let properties = area.properties;
    properties = properties.setIn(['landlordBool'], !landlordBool);
    
    projectActions.setProperties(properties);
    
    projectActions.unselectAll();

    projectActions.setAlterateState();
    // console.log(scene)
    let areas = scene.getIn(['layers', defaultlayerID, 'areas']); // get the areas
    let areasValues = areas.valueSeq();
    // console.log(areasValues)
    areasValues.forEach(({ id: areaID }) => {
      areaActions.selectArea(defaultlayerID, areaID);
    });
    projectActions.setAlterateState();

    setLandlordBool(!landlordBool);
  }

  let polygon = area.vertices.toArray().map(vertexID => {
    let {x, y} = layer.vertices.get(vertexID);
    return [x, y];
  });

  let polygonWithHoles = polygon;

  area.holes.forEach(holeID => {

    let polygonHole = layer.areas.get(holeID).vertices.toArray().map(vertexID => {
      let {x, y} = layer.vertices.get(vertexID);
      return [x, y];
    });

    polygonWithHoles = polygonWithHoles.concat(polygonHole.reverse());
  });

  let center = polylabel([polygonWithHoles], 1.0);
  
  // filter by the actual selected landlord area

  if (area.selected) {
    // let areaSize = areapolygon(polygon, false);

    // //subtract holes area
    // area.holes.forEach(areaID => {
    //   let hole = layer.areas.get(areaID);
    //   let holePolygon = hole.vertices.toArray().map(vertexID => {
    //     let {x, y} = layer.vertices.get(vertexID);
    //     return [x, y];
    //   });
    //   areaSize -= areapolygon(holePolygon, false);
    // });

    // renderedAreaSize = (
    //   <text x="0" y="0" transform={`translate(${center[0]} ${center[1]}) scale(1, -1)`} style={STYLE_TEXT}>
    //     {(areaSize / 10000 * 25).toFixed(0)} m{String.fromCharCode(0xb2)}
    //   </text>
    // )

    renderedAreaType = (
      <g id={"area_polygon-" + area.id} className="base" x="0" y="0" transform={`translate(${center[0]} ${center[1]}) scale(1, -1)`}>
        {/* <svg version="1.1" id="Layer_1" x="0px" y="0" viewBox="0 0 56.7 57" enable-background="new 0 0 56.7 57">
        <path class="retract" fill="none" stroke="#f1f1f1" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
          M36,6.3c5.7,2.1,10.2,6.2,12.9,11.1"/>
        <path fill="none" stroke="#f1f1f1" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
          M48.9,17.4c3.1,5.8,3.9,12.9,1.4,19.7c-4.6,12.4-18.4,18.8-30.8,14.3S0.7,33,5.2,20.5S23.6,1.7,36,6.3"/>
        <polyline class="tick" fill="none" stroke="#f1f1f1" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
          20.3,25.4 28.6,33.9 46.7,8.3 "/>
        </svg> */}
        <circle id={"area_circle-" + area.id} cx="-30" cy="0" className={landlordBool ? 'toggleLandlord' : 'toggleOffice' } r="16" onClick={changeAreaType.bind(this)}/>
        <text x="-30" y="0" style={STYLE_TEXT_ITALIC}>
        {"click"}
        </text>
        <text x="35" y="0" style={STYLE_TEXT}>
        {landlordBool ? "landlord area" : "office area"}
        </text>
        <style>
           { `.toggleLandlord { fill: #465767; stroke: black; }` }
           { `.toggleOffice { fill: #97c7bf; stroke: black; }` }
        </style>
      </g>
    )
  } else {
    
    renderedAreaType = (
      <g id={"area_polygon-" + area.id} className="base" x="0" y="0" transform={`translate(${center[0]} ${center[1]}) scale(1, -1)`}>
        <text x="0" y="0" style={STYLE_TEXT}>
        {landlordBool ? "landlord area" : "office area"}
        </text>
      </g>
    )
  }

  return (
      <g
      data-element-root
      data-prototype={area.prototype}
      data-id={area.id}
      data-selected={area.selected}
      data-layer={layer.id}
      >
      {rendered}
      {/* {renderedAreaSize} */}
      {renderedAreaType}
      </g>
  )

}

Area.propTypes = {
  area: PropTypes.object.isRequired,
  layer: PropTypes.object.isRequired,
  catalog: PropTypes.object.isRequired
};

Area.contextTypes = {
  itemsActions: PropTypes.object.isRequired,
  projectActions: PropTypes.object.isRequired,
  areaActions: PropTypes.object.isRequired,
};
